const mongoose = require('mongoose');

const paymentSchema = new mongoose.Schema({
    paymentDate: {
        type: Date,
        default: new Date()
    }
    ,paymentType: {
        type: String,
        required: [true, `Payment type is required`]
        //values: cash, paymaya, gcash, etc
    }
    ,paymentRefNo: {
        type: String
    }
    ,orderId: {
        type: String,
        required: [true, `Order ID is required.`]
    }
    ,customerId: {
        type: String,
        required: [true, `Customer ID is required`]
    },
    customerName: {
        type: String,
        required: [true, `Customer name is required.`]
    }
    ,paymentAmount: {
        type: Number,
        required: [true, `Payment Amount required`]
    }
});

module.exports = mongoose.model(`Payment`, paymentSchema);