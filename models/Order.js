const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, `Order total amount is required.`]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    orderItems: [{
        productId: {
            type: String,
            required: [true, `Product ID is required.`]
        },
        productName: {
            type: String,
            required: [true, `Product name is required.`]
        },
        productQty: {
            type: Number,
            required: [true, `Ordered product quantity is required.`]
        },
        productPrice: {
            type: Number,
            required: [true, `Product price is required.`]
        },
        subTotal: {
            type: Number,
            required: [true, `Order subtotal is required.`]
        }
    }],
    orderedBy: {
        type: String,
        required: [true, `Customer name is required.`]
    },
    orderedById: {
        type: String,
        required: [true, `Customer ID is required.`]
    },
    orderStatus: {
        type: String,
        default: `On-going`
        //other Status: Cancelled, Complete
    },
    paymentStatus: {
        type: String,
        default: `Pending`
        //other Status: Cancelled, Paid
    },
    orderFulfilled: {
        type: Date
    },
    paymentId: {
        type: String
    },
    paymentDate: {
        type: Date
    },
    paymentRefNo: {
        type: String
    }
},{timestamps:true});

module.exports = mongoose.model(`Order`, orderSchema);