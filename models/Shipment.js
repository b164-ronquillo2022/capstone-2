const mongoose = require('mongoose');

const shipmentSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: [true, `Order Id is required.`]
    }
    ,type: {
        type: String,
        required: [true, `Shipment type is required`]
        //values: lalamove, grab, fedEx, etc
    }
    ,status: {
        type: String,
        default: `For booking`,
        //values: for pickup, in-transit, delivered
    }
    ,pickupDate: {
        type: Date,
        required: [true, `Shipment pick-up date is required`]
    }
    ,receivedDate: {
        type: Date
    }
    ,address:{
        type: String,
        required: [true, `Delivery address is required.`]
    }
    ,refNo: {
        type: String
    }
});

module.exports = mongoose.model(`Shipment`, shipmentSchema);