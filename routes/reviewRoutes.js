const express = require('express');
const router = express.Router();

const { 
        addReview
        ,getUserReviews
        ,getAllReviews
        ,getReview
        ,editReview
        ,deleteReview
        ,searchReview
        //,updateReviewerName
} = require('./../controllers/reviewControllers');

const {verify, verifyAdmin, decode, verifyNonAdmin} = require('./../auth');

//add review - non-admin
router.post('/add-review/:productId', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await addReview(req.params.productId, userId, req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
    
})
//retrieve all reviews non-admin
router.get('/user-reviews/:userId', verify, async (req, res) => {
    try{
        //let userId = decode(req.headers.authorization).id;
        await getUserReviews(req.params.userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve all reviews admin only
router.get('/all-reviews', verifyAdmin, async (req, res) => {
    try{
        await getAllReviews().then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve single review
router.get('/review/:reviewId', verify, async (req, res) => {
    try{
        await getReview(req.params.reviewId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//edit reviews - non-admin
router.put('/edit-review/:reviewId', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        return await editReview(req.params.reviewId, userId, req.body).then(result => res.send(result));
    } catch (err) {
        res.status(500).json(err);
    }
})

//delete review
router.delete('/delete-review/:reviewId', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        return await deleteReview(req.params.reviewId, userId).then(result => res.send(result));
    }catch(err) {
        res.status(500).json(err);
    }
})

//search review
router.get('/search-reviews/:keyword', verifyAdmin, async (req, res) => {
    try{
        await searchReview(req.params.keyword).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//update reviewerName field if name is updated in user module
// router.patch('/update-reviewer-name/:userId/', verify, async (req, res) => {
//     try{
//         await updateReviewerName(req.params.userId, req.body.name).then(result => res.send(result));
//     } catch(err){
//         res.status(500).json(err);
//     }
// })

module.exports = router;