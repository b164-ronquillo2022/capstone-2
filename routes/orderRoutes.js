const express = require('express');
const router = express.Router();
const {verify, verifyAdmin, verifyNonAdmin, decode} = require('./../auth');

const {
    createOrder
    ,getUserOrders
    ,getAllOrders
    ,getPendingOrders
    ,updateOrderStatus
    ,updatePaymentStatus
    ,getOrder
    ,searchOrder
    ,searchUserOrder
    ,searchUserOrderId
    //,sendMail
} = require('./../controllers/orderControllers');

//Create order - non-admin user checkout
router.post('/create-order', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await createOrder(req.body, userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Retrieve order history for a single user
router.get('/order-history/:userId', verify, async (req, res) => {
    try{
        //let userId = decode(req.headers.authorization).id;
        await getUserOrders(req.params.userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Retrieve own orders
router.get('/my-orders', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await getUserOrders(userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Retrieve all orders - admin only
router.get('/all-orders', verifyAdmin, async (req, res) => {
    try{
        await getAllOrders().then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//FEATURES
//retrieve all pending orders (admin only)
router.get('/pending-orders', verifyAdmin, async (req, res) => {
    try{
        await getPendingOrders().then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//change order status (add orderFulfilled)
router.patch('/order-status/:orderId', verifyAdmin, async (req, res) => {
    try{
        await updateOrderStatus(req.params.orderId, req.body.orderStatus).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})


//change payment status
router.patch('/payment-status/:orderId', verify, async (req, res) => {
    try{
        let {paymentStatus, paymentId, refNo} = req.body;
        await updatePaymentStatus(req.params.orderId, paymentStatus, paymentId, refNo).then(result=>res.send(result));
    }catch(err){
        return err;
    }
})

//retrieve single order details
router.get('/order/:orderId', verify, async (req, res) => {
    try{
        await getOrder(req.params.orderId).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//search order by product or reviewer
router.get('/search-order/:keyword', verifyAdmin, async (req, res) => {
    try{
        await searchOrder(req.params.keyword).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//search user order by product name
router.get('/search-my-order/:keyword', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await searchUserOrder(req.params.keyword, userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//search order Id, display if own only
router.get('/search-my-order-id/:orderId', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await searchUserOrderId(req.params.orderId, userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//send mail --- not working
// router.get('/send-mail/', verify, async (req, res) => {
//     try{
//         await sendMail().then(result => res.send(result));
//     }catch(err){
//         res.status(500).json(err);
//     }
// })

module.exports = router;