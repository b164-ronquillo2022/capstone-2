const Payment = require('./../models/Payment');
const { updatePaymentStatus, getOrder } = require('./orderControllers');

module.exports.pay = async (userId, orderId, reqBody) => {
    try{
        let order = await getOrder(orderId).then(result=>result);

        //check if user made order, if not, not allowed to pay
        if (order.orderedById == userId){
            let newPayment = new Payment({
                paymentType: reqBody.paymentType,
                paymentRefNo: reqBody.paymentRefNo,
                orderId: orderId,
                customerId: userId,
                customerName: reqBody.customerName,
                paymentAmount: reqBody.paymentAmount
            })

            return await newPayment.save().then(result => {
                //successful save
                if ( result !== null){
                    return updatePaymentStatus(orderId, `Paid`, result._id, result.paymentRefNo);
                } else {
                    return {message: `Payment error.`}
                }
            })
        }else {
            return {message: `User not eligible to pay for this order.`}
        }
    } catch (err){
        console.log(err);
        throw err;
    }
}

module.exports.getAllPayments = async () => {
    try{
        return await Payment.find().then(result=>result?result:{message:`No records retrieved.`});
    }catch(err){
        throw err;
    }
}

module.exports.searchPayment = async (keyword) => {
    try{
        let search = 
        {
            $or: [
                { orderId: keyword },
                { customerId: keyword }
            ]
        }

        return await Payment.find(search).then(result => {
            
            if (result.length <= 0){
                return Payment.findById(keyword).then(result=>result);
            } else {
                return result;
            }
            
        });
    }catch(err){
        throw err;
    }
}

module.exports.searchRefNo = async (refNo) => {
    try{
        return await Payment.find({paymentRefNo: refNo}).then(result=>result);
    }catch(err){
        throw err;
    }
}

module.exports.searchMyRef = async (refNo, userId) => {
    try{
        return await Payment.find({paymentRefNo: refNo}).then(result=>{

            let tempArr = [];

            if (result.length > 0){
                result.forEach(payment=> {
                    if (payment.customerId === userId){
                        tempArr.push(payment);
                    }
                })
            }

            return tempArr;
        });
    }catch(err){
        throw err;
    }
}

module.exports.getUserPayments = async (userId) => {
    try{
        return await Payment.find({customerId: userId}).then(result => {
            if ( result === null ){
                return {message: `No payments made by this user.`};
            } else {
                return result;
            }
        })
    }catch(err){
        throw(err);
    }
}

module.exports.getPayment = async (paymentId) => {
    try{
        return await Payment.findById(paymentId).then(result => {
            if ( result === null ){
                return {message: `Payment details not found.`};
            } else {
                return result;
            }
        });
    }catch(err){
        throw(err);
    }
}

module.exports.searchCustomer = async (keyword) => {
    try{
        return await Payment.find({ customerName: {$regex: keyword, $options: "i"} }).then(result => result);
    }catch(err){
        throw err;
    }
}

module.exports.searchCustomerOrder = async (keyword, userId) => {
    try{

        return await Payment.find({orderId: keyword}).then(result=>{

            let tempArr = [];

            if (result.length > 0){
                result.forEach(payment=> {
                    if (payment.customerId === userId){
                        tempArr.push(payment);
                    }
                })
            }

            return tempArr;

        });
    }catch(err){
        throw err;
    }
}